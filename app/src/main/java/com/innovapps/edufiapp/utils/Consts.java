package com.innovapps.edufiapp.utils;

/**
 * Created by rolangom on 11/15/14.
 */
public class Consts {
    public final static String NETWORK_ID = "network_id";


    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    private final static int
            CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    // SHARED PREFERENCES
    public static final String APP_PREFERENCES = "edufiapp_prefereces";
    public static final String REGISTERED_STR_TAG = "edufiapp_registered";
    public static final String GCM_TOKEN = "gcm_token";
    public static final String SERVICE_STATUS = "service_status";

    // Push Notification
    public static final String DATA_PUSHED = "data_pushed";
    public static final String REG_ID_PREF = "edufiapp_reg_id";

    public static final String PROJECT_NUMBER = "947691294306";

    public static final long SERVICE_RESPONSE_TIME_LIMIT = 15000; // milliseconds

    public static final String BASE_URL = "http://54.187.24.113/edufiapp/api/";

    public static final int REQUEST_IMAGE_CAPTURE = 1;

    public static final int WEEKLY = 0;
    public static final int BIWEEKLY = 1;
    public static final int MONTHLY = 2;
}
