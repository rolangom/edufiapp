package com.innovapps.edufiapp.utils;

import android.app.ProgressDialog;
import android.content.Context;

import com.innovapps.edufiapp.R;

/**
 * Created by rolangom on 11/15/14.
 */
public class Funcs {

    public static class ProgressDialogManager {
        Context mContext;
        ProgressDialog mProgressDialog;

        public ProgressDialogManager(Context mContext) {
            this.mContext = mContext;
        }
        public void show() {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(mContext);
                String message = mContext.getString(R.string.please_wait);
                mProgressDialog.setMessage(message);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setIndeterminate(true);
            }
            mProgressDialog.show();
        }
        public void dismiss() {
            mProgressDialog.dismiss();
        }
    }
}
