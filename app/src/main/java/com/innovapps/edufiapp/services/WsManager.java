package com.innovapps.edufiapp.services;

import android.util.Log;

import com.innovapps.edufiapp.models.Objective;
import com.innovapps.edufiapp.models.User;
import com.innovapps.edufiapp.utils.Consts;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by rolangom on 11/15/14.
 */
public class WsManager {

    private static AsyncHttpClient mClient = new AsyncHttpClient();

    public static void registerUser(User user, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams();
        params.put("email", user.getEmail());
        params.put("name", user.getName());
        params.put("lastname", user.getLastName());
        params.put("fbid", user.getFbid());
        params.put("device_type", "android");
        params.put("handle", user.getHandle());

        Log.d("WsManager -> registerUser", user.toString());

        mClient.addHeader("Content-Type", "application/x-www-form-urlencoded");
        mClient.post(Consts.BASE_URL + "register", params, responseHandler);
    }

    public static void loginUser(User user, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams();
        params.put("email", user.getEmail());
        params.put("fbid", user.getFbid());

        Log.d("WsManager -> loginUser", user.toString());

        mClient.addHeader("Content-Type", "application/x-www-form-urlencoded");
        mClient.post(Consts.BASE_URL + "login", params, responseHandler);
    }

    public static void getAdvices(AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams();
        mClient.get(Consts.BASE_URL + "advices", params, responseHandler);
    }

    public static void addObjective(Objective objective, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams();
        params.put("user_id", objective.getUserId());
        params.put("title", objective.getTitle());
        params.put("description", objective.getDescription());
        params.put("amount", objective.getAmount());
        params.put("saving_frec", objective.getSavingFreq());
        params.put("buy_date", objective.getBuyDateStr());

        Log.d("WsManager -> addObjective", objective.toString());

        mClient.post(Consts.BASE_URL + "addObjectives", params, responseHandler);
    }
}
