package com.innovapps.edufiapp.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.innovapps.edufiapp.R;
import com.innovapps.edufiapp.utils.Consts;
import com.innovapps.edufiapp.activities.MainActivity;
import com.innovapps.edufiapp.receivers.GcmBroadcastReceiver;

/**
 * Created by rolangom on 11/15/14.
 */
public class GcmIntentService extends IntentService {

    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;

    SharedPreferences appValues;

    public GcmIntentService() {
        super("GcmIntentService");

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        appValues = this.getSharedPreferences(Consts.APP_PREFERENCES,
                Context.MODE_MULTI_PROCESS);

        if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                    .equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                    .equals(messageType)) {
                sendNotification("Deleted messages on server: "
                        + extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                    .equals(messageType)) {
                Log.d("Bolita", "sector: " + extras.getString("sector"));

//                showToast( "sector: "+extras.getString("sector"));

//                ServiceRequested sr = new ServiceRequested();
//                sr.id = Long.parseLong(extras.getString("id"));
//                sr.sector = extras.getString("sector");
//                sr.address = extras.getString("address");
//                sr.reference = extras.getString("reference");
//                sr.lat = Double.parseDouble(extras.getString("lat"));
//                sr.lng = Double.parseDouble(extras.getString("lng"));
//                sr.tel = extras.getString("tel");
//                sr.setAllData(extras.getString("sector"), extras.getString("address"), extras.getString("reference"),
//                         extras.getDouble("lat"), extras.getDouble("lng"), extras.getString("tel"));

                Intent launchIntent = new Intent(this, MainActivity.class);
                launchIntent.putExtra(Consts.DATA_PUSHED, new String()); // TODO

                launchIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(launchIntent);
//                sendNotification("Message received: " + extras.toString());
            }
            // Release the wake lock provided by the WakefulBroadcastReceiver.
            GcmBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.ic_launcher).setAutoCancel(true)
                .setContentTitle("TODO TITLE")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private void showToast(final String message){
        // create a handler to post messages to the main thread
        Handler mHandler = new Handler(getMainLooper());
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
