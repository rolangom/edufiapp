package com.innovapps.edufiapp.models;

import com.innovapps.edufiapp.models.enums.SavingFreq;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rolangom on 11/15/14.
 */
public class Objective {
    String id;
    String userId;
    String title;
    String description;
    String image;
    double amount;
    SavingFreq savingFreq;
    Date buyDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public SavingFreq getSavingFreq() {
        return savingFreq;
    }

    public void setSavingFreq(SavingFreq savingFreq) {
        this.savingFreq = savingFreq;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public String getBuyDateStr() {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return fmt.format(buyDate);
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    @Override
    public String toString() {
        return String.format("id: %s, userId: %s, title: %s, description: %s, amount: %s, savingFreq: %s, buyDate: %s"
                , id, userId, title, description, amount, savingFreq.ordinal(), getBuyDateStr());
    }
}
