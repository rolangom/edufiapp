package com.innovapps.edufiapp.models;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rolangom on 11/15/14.
 */
public class User {

    String id;
    String fbid;
    String name;
    String lastName;
    String email;
    String deviceType;
    String handle;
    Date registrationDate;
    String avatarUrl;

    public User(String fbid, String name, String email, String avatarUrl) {
        this.fbid = fbid;
        this.name = name.substring(0, name.indexOf(" "));
        this.lastName = name.substring(name.indexOf(" ") + 1);
        this.email = email;
        this.avatarUrl = avatarUrl;
        registrationDate = new Date();
    }

    public User() {
        registrationDate = new Date();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getFbid() {
        return fbid;
    }

    public void setFbid(String fbid) {
        this.fbid = fbid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public String getRegistrationDateStr() {
        SimpleDateFormat fmt = new SimpleDateFormat("y-MM-d");
        return fmt.format(registrationDate);
    }

    @Override
    public String toString() {
        return String.format("id: %s, fbid: %s, name: %s, lastname: %s, email: %s", id, fbid, name, lastName, email);
    }
}
