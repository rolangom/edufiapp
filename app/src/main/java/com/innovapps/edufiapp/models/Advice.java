package com.innovapps.edufiapp.models;

import java.util.Date;

/**
 * Created by rolangom on 11/15/14.
 */
public class Advice {

    String id;
    String title;
    String description;
    String autor;
    String date;

    public Advice(String id, String title, String description, String autor, String date) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.autor = autor;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getAutor() {
        return autor;
    }

    public String getDate() {
        return date;
    }
}
