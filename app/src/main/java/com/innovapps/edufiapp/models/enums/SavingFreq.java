package com.innovapps.edufiapp.models.enums;

/**
 * Created by rolangom on 11/16/14.
 */
public enum SavingFreq {
    WEEKLY,
    BIWEEKLY,
    MONTHLY;
}
