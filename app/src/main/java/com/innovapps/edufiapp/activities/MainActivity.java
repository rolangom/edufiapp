package com.innovapps.edufiapp.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnRequestSocialPersonCompleteListener;
import com.github.gorbin.asne.core.persons.SocialPerson;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.innovapps.edufiapp.R;
import com.innovapps.edufiapp.fragments.AdviceListFragment;
import com.innovapps.edufiapp.fragments.ObjectivesFragment;
import com.innovapps.edufiapp.services.WsManager;
import com.innovapps.edufiapp.utils.Consts;
import com.innovapps.edufiapp.adapters.MainPagerAdapter;
import com.innovapps.edufiapp.fragments.ProfileFragment;
import com.innovapps.edufiapp.models.User;
import com.innovapps.edufiapp.utils.GcmControl;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class MainActivity extends FragmentActivity implements OnRequestSocialPersonCompleteListener {

    public static SocialNetworkManager socialNetworkManager;
    // Handle to SharedPreferences for this app
    private SharedPreferences mPrefs;

    static String sGcmRegId;
    int mNetworkId;
    ProfileFragment mProfileFragment;
    AdviceListFragment mAdviceListFragment;
    ObjectivesFragment mObjectivesFragment;

    SocialNetwork mSocialNetwork;
    public static User sUser;

    // Handle to a SharedPreferences editor
    private SharedPreferences.Editor mEditor;
    private GoogleCloudMessaging mGcm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();

        if (extras.containsKey(Consts.NETWORK_ID))
            mNetworkId = extras.getInt(Consts.NETWORK_ID);

        initUi();
        initModule();

        mPrefs = getSharedPreferences(Consts.APP_PREFERENCES,
                Context.MODE_MULTI_PROCESS);
        // Get an editor
        mEditor = mPrefs.edit();
        getRegId();
    }

    void initUi() {
        // Initialize the ViewPager and set an adapter
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        MainPagerAdapter pagerAdapter = new MainPagerAdapter(getSupportFragmentManager());

        mAdviceListFragment = new AdviceListFragment(getString(R.string.advices));
        pagerAdapter.addFragment(mAdviceListFragment);

        mProfileFragment = new ProfileFragment(getString(R.string.profile));
        pagerAdapter.addFragment(mProfileFragment);

        mObjectivesFragment = new ObjectivesFragment(getString(R.string.objectives));
        pagerAdapter.addFragment(mObjectivesFragment);

        pager.setAdapter(pagerAdapter);

        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(pager);
        tabs.setIndicatorColorResource(R.color.brand_green);
    }

    void initModule() {
        mSocialNetwork = socialNetworkManager.getSocialNetwork(mNetworkId);
        mSocialNetwork.setOnRequestCurrentPersonCompleteListener(this);
        mSocialNetwork.requestCurrentPerson();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.logout) {
            logout();
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }

    void logout() {
        mSocialNetwork.logout();
        Intent i = new Intent(this, StartActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onRequestSocialPersonSuccess(int i, SocialPerson socialPerson) {
        sUser = new User(socialPerson.id, socialPerson.name, socialPerson.email, socialPerson.avatarURL);
        mProfileFragment.setUser(sUser);
        StartActivity.instance.finish();
        registerInBackend();
    }

    @Override
    public void onError(int i, String s, String s2, Object o) {
        Toast.makeText(this, "Error "+s+", s2", Toast.LENGTH_LONG).show();
    }

    // You need to do the Play Services APK check here too.
    @Override
    protected void onResume() {
        super.onResume();
        GcmControl.checkPlayServices(this);
    }

    void setAsRegistered() {
        mEditor.putBoolean(Consts.REGISTERED_STR_TAG, true);
        mEditor.commit();
    }

    boolean isRegistered() {
        return mPrefs.contains(Consts.REGISTERED_STR_TAG);
    }

    void loginInBackend() {
        WsManager.loginUser(sUser, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.i(this.getClass().getName(), "Respose loginUser: " + response.toString());
                try {
                    if (response.has("id")) {
                        String id = response.getString("id");
                        sUser.setId(id);
                    } else
                        showDialogError(getString(R.string.connection_error_invalid_account));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void registerInBackend() {
        if (sGcmRegId == null) {
            getRegId();
            return;
        }
        if (sUser == null) {
            Toast.makeText(this, R.string.connection_error_invalid_account, Toast.LENGTH_LONG).show();
            return;
        }
        sUser.setHandle(sGcmRegId);

        if (isRegistered())
            loginInBackend();
        else {
            WsManager.registerUser(sUser, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.i(this.getClass().getName(), "Respose: " + response.toString());

                    try {
                        if (response.has("success")) {
                            setAsRegistered();
                            loginInBackend();
                        } else {
                            String errorText = response.getJSONObject("error").getString("text");
                            if (errorText.contains("registrado")) {
                                setAsRegistered();
                                loginInBackend();
                                return;
                            }
                            showDialogError(errorText);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    void showDialogError(String error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.error)
                .setMessage(error);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    public void getRegId(){
        if (mPrefs.contains(Consts.REG_ID_PREF)){
            sGcmRegId = mPrefs.getString(Consts.REG_ID_PREF, "");
            registerInBackend();
        }else // if we don't have the reg ID
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String msg = "";
                    try {
                        if (mGcm == null) {
                            mGcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                        }
                        sGcmRegId = mGcm.register(Consts.PROJECT_NUMBER);
                        msg = "Device registered, registration ID=" + sGcmRegId;
                        Log.i("GCM", msg);

                        mEditor.putString("regId", sGcmRegId);
                    } catch (IOException ex) {
                        msg = "Error :" + ex.getMessage();
                    }
                    return msg;
                }

                @Override
                protected void onPostExecute(String msg) {
                    Log.i("GCM",  msg);
                    registerInBackend();
                }
            }.execute(null, null, null);
    }
}
