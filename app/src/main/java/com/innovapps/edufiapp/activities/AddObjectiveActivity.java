package com.innovapps.edufiapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.innovapps.edufiapp.R;
import com.innovapps.edufiapp.models.Objective;
import com.innovapps.edufiapp.models.enums.SavingFreq;
import com.innovapps.edufiapp.services.WsManager;
import com.innovapps.edufiapp.utils.Consts;
import com.innovapps.edufiapp.utils.Funcs;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AddObjectiveActivity extends Activity {

    Funcs.ProgressDialogManager mProgressDialogManager;

    EditText mTitleView, mDescrView, mAmountView;
    Spinner mSavingFreqSpinner;
    DatePicker mBuyDatePicker;

    ImageButton mCameraBtn;

    Objective mObjective;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_objective);

        mProgressDialogManager = new Funcs.ProgressDialogManager(this);
        initUi();
    }

    void initUi() {
        mTitleView = (EditText) findViewById(R.id.title);
        mDescrView = (EditText) findViewById(R.id.description);
        mAmountView = (EditText) findViewById(R.id.amount);
        mSavingFreqSpinner = (Spinner) findViewById(R.id.spinner);
        mBuyDatePicker = (DatePicker) findViewById(R.id.datePicker);
        mCameraBtn = (ImageButton) findViewById(android.R.id.button1);

        mBuyDatePicker.setMinDate(System.currentTimeMillis() - 1000);
        mSavingFreqSpinner.setAdapter(ArrayAdapter.createFromResource(this, R.array.saving_frequency, android.R.layout.simple_spinner_dropdown_item));

        mCameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCamera();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_objective, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.save) {
            save();
            return true;
        } else if (id == android.R.id.home) {
            backToHome();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void save() {
        boolean isValid = true;
        if (mTitleView.getText().length() == 0) {
            isValid = false;
            setErrorMsg(mTitleView);
        }
        if (mDescrView.getText().length() == 0) {
            isValid = false;
            setErrorMsg(mDescrView);
        }
        if (mAmountView.getText().length() == 0) {
            isValid = false;
            setErrorMsg(mAmountView);
        }

        if (isValid) {
            mObjective = new Objective();
            mObjective.setTitle(mTitleView.getText().toString());
            mObjective.setDescription(mDescrView.getText().toString());
            mObjective.setAmount(Double.parseDouble(mAmountView.getText().toString()));
            mObjective.setSavingFreq(SavingFreq.values()[mSavingFreqSpinner.getSelectedItemPosition()]);
            mObjective.setUserId(MainActivity.sUser.getId());
            mObjective.setBuyDate(getDatePickerDate(mBuyDatePicker));

            mProgressDialogManager.show();
            WsManager.addObjective(mObjective, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    mProgressDialogManager.dismiss();
                    Log.i(this.getClass().getName(), "Respose: " + response.toString());
                    Toast.makeText(getApplicationContext(), "Respose: " + response.toString(), Toast.LENGTH_LONG).show();
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    mProgressDialogManager.dismiss();
                    Log.i(this.getClass().getName(), "Respose: " + errorResponse.toString());
                    Toast.makeText(getApplicationContext(), "Respose: " + errorResponse.toString(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    Date getDatePickerDate(DatePicker picker) {
        int day = picker.getDayOfMonth();
        int month = picker.getMonth();
        int year = picker.getYear();

        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day);

        return cal.getTime();
    }

    void setErrorMsg(EditText editText) {
        String message = getString(R.string.required_field);
        editText.setError(message);
    }

    void backToHome() {
        finish();
    }

    void launchCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, Consts.REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Consts.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            mCameraBtn.setImageBitmap(imageBitmap);
        }
    }
}
