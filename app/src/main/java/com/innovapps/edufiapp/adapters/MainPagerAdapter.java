package com.innovapps.edufiapp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.innovapps.edufiapp.fragments.StateFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rolangom on 11/15/14.
 */
public class MainPagerAdapter extends FragmentStatePagerAdapter {
    List<StateFragment> mList;

    public MainPagerAdapter (FragmentManager fm) {
        super(fm);
        mList = new ArrayList<StateFragment>();
    }

    public void addFragment(StateFragment fragment) {
        mList.add(fragment);
    }

    @Override
    public Fragment getItem(int i) {
        return mList.get(i);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mList.get(position).getTitle();
    }
}
