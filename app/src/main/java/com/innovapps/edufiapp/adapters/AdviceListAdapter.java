package com.innovapps.edufiapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.innovapps.edufiapp.R;
import com.innovapps.edufiapp.models.Advice;
import com.innovapps.edufiapp.utils.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rolangom on 11/15/14.
 */
public class AdviceListAdapter extends BaseAdapter {

    List<Advice> mList;
    LayoutInflater mInflater;

    public AdviceListAdapter (Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mList = new ArrayList<Advice>();
    }

    public void addRange(List<Advice> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Advice getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null)
            view = mInflater.inflate(R.layout.item_list_advice, viewGroup, false);

        TextView titleView = ViewHolder.get(view, R.id.title);
        TextView contentView = ViewHolder.get(view, android.R.id.content);
        TextView autorView = ViewHolder.get(view, R.id.autor);
        TextView dateView = ViewHolder.get(view, R.id.date);

        Advice item = getItem(i);

        titleView.setText(item.getTitle());
        contentView.setText(item.getDescription());
        autorView.setText(item.getAutor());
        dateView.setText(item.getDate());

        return view;
    }
}
