package com.innovapps.edufiapp.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.innovapps.edufiapp.R;
import com.innovapps.edufiapp.activities.AddObjectiveActivity;
import com.melnykov.fab.FloatingActionButton;

/**
 * A simple {@link Fragment} subclass.
 */
public class ObjectivesFragment extends StateFragment {

    View mView;
    ListView mListView;
    FloatingActionButton mAddBtn;

    public ObjectivesFragment(String title) {
        super(title);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_objectives, container, false);
        initUi();
        return mView;
    }

    void initUi() {
        mListView = (ListView) mView.findViewById(android.R.id.list);
        mAddBtn = (FloatingActionButton) mView.findViewById(R.id.fab);
        mAddBtn.attachToListView(mListView);

        mAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startAddOjective();
            }
        });
    }

    void startAddOjective() {
        Intent i = new Intent(getActivity(), AddObjectiveActivity.class);
        startActivity(i);
    }

}
