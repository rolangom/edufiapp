package com.innovapps.edufiapp.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by rolangom on 11/15/14.
 */
public abstract class StateFragment extends Fragment {
    String mTitle;

    public StateFragment(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }
}
