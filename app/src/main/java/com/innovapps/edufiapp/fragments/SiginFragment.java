package com.innovapps.edufiapp.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.innovapps.edufiapp.R;

import com.innovapps.edufiapp.utils.Consts;
import com.innovapps.edufiapp.utils.Funcs;
import com.innovapps.edufiapp.activities.MainActivity;
import com.innovapps.edufiapp.activities.StartActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A fragment with a Google +1 button.
 */
public class SiginFragment extends Fragment implements SocialNetworkManager.OnInitializationCompleteListener,
        OnLoginCompleteListener {

    View mView;

    Button mLoginFbBtn;

    ArrayList<String> fbScope;
    FacebookSocialNetwork fbNetwork;
    Funcs.ProgressDialogManager mProgressDialogManager;

    public SiginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_sigin, container, false);

        initUi();
        initModule();

        return mView;
    }

    void initUi() {
        mLoginFbBtn = (Button) mView.findViewById(R.id.facebook);
        mLoginFbBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fbLogin();
            }
        });

        mProgressDialogManager = new Funcs.ProgressDialogManager(getActivity());
    }

    void initModule() {
        fbScope = new ArrayList<String>();
        fbScope.addAll(Arrays.asList("public_profile, email"));
        fbNetwork = new FacebookSocialNetwork(this, fbScope);

        //Use manager to manage SocialNetworks
        MainActivity.socialNetworkManager = (SocialNetworkManager) getFragmentManager().findFragmentByTag(StartActivity.SOCIAL_NETWORK_TAG);

        if (MainActivity.socialNetworkManager == null) {
            MainActivity.socialNetworkManager = new SocialNetworkManager();
            MainActivity.socialNetworkManager.addSocialNetwork(fbNetwork);

            //Initiate every network from socialNetworkManager
            getFragmentManager().beginTransaction().add(MainActivity.socialNetworkManager, StartActivity.SOCIAL_NETWORK_TAG).commit();
            MainActivity.socialNetworkManager.setOnInitializationCompleteListener(this);
        } else {
            //if manager exist - get and setup login only for initialized SocialNetworks
            if (!MainActivity.socialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
                List<SocialNetwork> socialNetworks = MainActivity.socialNetworkManager.getInitializedSocialNetworks();
                for (SocialNetwork socialNetwork : socialNetworks) {
                    socialNetwork.setOnLoginCompleteListener(this);
                    initSocialNetwork(socialNetwork);
                }
            }
        }
    }
    private void initSocialNetwork(SocialNetwork socialNetwork){
        if(socialNetwork.isConnected()){
            switch (socialNetwork.getID()){
                case FacebookSocialNetwork.ID:
                    fbLogin();
                    break;
            }
        }
    }

    void fbLogin() {
        int networkId = FacebookSocialNetwork.ID;
        SocialNetwork socialNetwork = MainActivity.socialNetworkManager.getSocialNetwork(networkId);
        if(!socialNetwork.isConnected()) {
            if(networkId != 0) {
                socialNetwork.requestLogin();
                mProgressDialogManager.show();
            } else {
                Toast.makeText(getActivity(), "Wrong networkId", Toast.LENGTH_LONG).show();
            }
        } else {
            startMainActivity();
        }
    }


    @Override
    public void onSocialNetworkManagerInitialized() {
        //when init SocialNetworks - get and setup login only for initialized SocialNetworks
        for (SocialNetwork socialNetwork : MainActivity.socialNetworkManager.getInitializedSocialNetworks()) {
            socialNetwork.setOnLoginCompleteListener(this);
            initSocialNetwork(socialNetwork);
        }
    }
    @Override
    public void onLoginSuccess(int networkId) {
        mProgressDialogManager.dismiss();
        startMainActivity();
    }

    @Override
    public void onError(int networkId, String requestID, String errorMessage, Object data) {
        mProgressDialogManager.dismiss();
        Toast.makeText(getActivity(), "ERROR: " + errorMessage, Toast.LENGTH_LONG).show();
    }

    void startMainActivity() {
        Intent i = new Intent(getActivity(), MainActivity.class);
        i.putExtra(Consts.NETWORK_ID, FacebookSocialNetwork.ID);
        startActivity(i);
    }
}
