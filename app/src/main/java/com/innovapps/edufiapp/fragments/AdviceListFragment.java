package com.innovapps.edufiapp.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.innovapps.edufiapp.R;
import com.innovapps.edufiapp.adapters.AdviceListAdapter;
import com.innovapps.edufiapp.models.Advice;
import com.innovapps.edufiapp.services.WsManager;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdviceListFragment extends StateFragment {

    ListView mListView;
    AdviceListAdapter mAdapter;

    public AdviceListFragment(String title) {
        super(title);
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_advice_list, container, false);

        mListView = (ListView) view.findViewById(android.R.id.list);
        mAdapter = new AdviceListAdapter(getActivity());
        mListView.setAdapter(mAdapter);
        initData();

        return view;
    }

    void initData() {
        WsManager.getAdvices(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.d(this.getClass().getName(), response.toString());
                try {
                    JSONArray advicesJarr = response.getJSONArray("advices");

                    List<Advice> advices = new ArrayList<Advice>();
                    for (int i = 0; i < advicesJarr.length(); i++) {
                        JSONObject adviceJobj = advicesJarr.getJSONObject(i);
                        Advice advice = new Advice(
                                adviceJobj.getString("id"),
                                adviceJobj.getString("title"),
                                adviceJobj.getString("description"),
                                adviceJobj.getString("date"),
                                adviceJobj.getString("user_id").equals("1") ? "admin" : "user");
                        advices.add(advice);
                    }
                    mAdapter.addRange(advices);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
