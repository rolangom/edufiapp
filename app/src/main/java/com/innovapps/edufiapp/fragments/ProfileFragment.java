package com.innovapps.edufiapp.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.innovapps.edufiapp.R;
import com.innovapps.edufiapp.models.User;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends StateFragment {

    View mView;
    ImageView mProfileView;
    TextView mFullNameView;
    TextView mEmailView;

    User mUser;

    public ProfileFragment(String title) {
        super(title);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_profile, container, false);
        initUi();
        return mView;
    }

    void initUi() {
        mProfileView = (ImageView) mView.findViewById(android.R.id.icon);
        mFullNameView = (TextView) mView.findViewById(R.id.fullname);
        mEmailView = (TextView) mView.findViewById(R.id.email);
    }

    void initData() {
        if (mUser !=null) {
            mFullNameView.setText(mUser.getName());
            mEmailView.setText(mUser.getEmail());
            Picasso.with(getActivity())
                    .load(mUser.getAvatarUrl())
                    .into(mProfileView);
        }
    }

    public void setUser(User mUser) {
        this.mUser = mUser;
        initData();
    }
}
